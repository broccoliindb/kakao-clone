# Kakao-clone

HTML, CSS연습을 위한 kakaotalk clone coding

width를 470px 이하로 적용.

https://kakaotalk-clone-yong.netlify.app

## 화면

### 친구
![friend](./covers/friend.png)
### 채팅리스트
![chatList](./covers/chatList.png)
### 채팅
![chat](./covers/chat.png)
### 더보기
![more](./covers/more.png)
### 검색
![search](./covers/search.png)
### 설정
![setting](./covers/setting.png)

